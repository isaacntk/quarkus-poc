package poc.test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class GreetingServiceControllerTest {

    @Test
    void testGreeting() {
        given()
                .when().get("/obi")
                .then()
                .statusCode(200)
                .body("message", is("Hello there obi, you're a bold one"));
    }
}
