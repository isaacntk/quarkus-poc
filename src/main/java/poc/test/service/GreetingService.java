package poc.test.service;

import org.springframework.stereotype.Service;
import poc.test.model.Greeting;

@Service
public class GreetingService {
    public Greeting getMessage(String name) {
        return new Greeting("Hello there " + name + ", you're a bold one");
    }
}
